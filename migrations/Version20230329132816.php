<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230329132816 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE article_ajoute_liste_course DROP FOREIGN KEY FK_839457EC6B01D708');
        $this->addSql('ALTER TABLE article_ajoute_liste_course ADD CONSTRAINT FK_839457EC6B01D7086B01D708 FOREIGN KEY (listecourse_id) REFERENCES liste_course (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE article_ajoute_liste_course DROP FOREIGN KEY FK_839457EC6B01D7086B01D708');
        $this->addSql('ALTER TABLE article_ajoute_liste_course ADD CONSTRAINT FK_839457EC6B01D708 FOREIGN KEY (listecourse_id) REFERENCES liste_course (id)');
    }
}
