<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230307102653 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE liste_course ADD utilisateur_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE liste_course ADD CONSTRAINT FK_27EF1A82FB88E14F FOREIGN KEY (utilisateur_id) REFERENCES utilisateur (id)');
        $this->addSql('CREATE INDEX IDX_27EF1A82FB88E14F ON liste_course (utilisateur_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE liste_course DROP FOREIGN KEY FK_27EF1A82FB88E14F');
        $this->addSql('DROP INDEX IDX_27EF1A82FB88E14F ON liste_course');
        $this->addSql('ALTER TABLE liste_course DROP utilisateur_id');
    }
}
