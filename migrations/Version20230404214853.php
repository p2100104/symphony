<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230404214853 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE liste_course_utilisateur (liste_course_id INT NOT NULL, utilisateur_id INT NOT NULL, INDEX IDX_A37D05EB4680FCB (liste_course_id), INDEX IDX_A37D05EBFB88E14F (utilisateur_id), PRIMARY KEY(liste_course_id, utilisateur_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE liste_course_utilisateur ADD CONSTRAINT FK_A37D05EB4680FCB FOREIGN KEY (liste_course_id) REFERENCES liste_course (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE liste_course_utilisateur ADD CONSTRAINT FK_A37D05EBFB88E14F FOREIGN KEY (utilisateur_id) REFERENCES utilisateur (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE utilisateur_liste_course DROP FOREIGN KEY FK_4E1D3E71FB88E14F');
        $this->addSql('ALTER TABLE utilisateur_liste_course DROP FOREIGN KEY FK_4E1D3E714680FCB');
        $this->addSql('DROP TABLE utilisateur_liste_course');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE utilisateur_liste_course (utilisateur_id INT NOT NULL, liste_course_id INT NOT NULL, INDEX IDX_4E1D3E71FB88E14F (utilisateur_id), INDEX IDX_4E1D3E714680FCB (liste_course_id), PRIMARY KEY(utilisateur_id, liste_course_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE utilisateur_liste_course ADD CONSTRAINT FK_4E1D3E71FB88E14F FOREIGN KEY (utilisateur_id) REFERENCES utilisateur (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE utilisateur_liste_course ADD CONSTRAINT FK_4E1D3E714680FCB FOREIGN KEY (liste_course_id) REFERENCES liste_course (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE liste_course_utilisateur DROP FOREIGN KEY FK_A37D05EB4680FCB');
        $this->addSql('ALTER TABLE liste_course_utilisateur DROP FOREIGN KEY FK_A37D05EBFB88E14F');
        $this->addSql('DROP TABLE liste_course_utilisateur');
    }
}
