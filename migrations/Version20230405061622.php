<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230405061622 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE article (id INT AUTO_INCREMENT NOT NULL, type_id INT DEFAULT NULL, nom VARCHAR(255) NOT NULL, prix DOUBLE PRECISION NOT NULL, image LONGBLOB DEFAULT NULL, INDEX IDX_23A0E66C54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE article_ajoute_liste_course (id INT AUTO_INCREMENT NOT NULL, article_id INT DEFAULT NULL, listecourse_id INT DEFAULT NULL, quantite INT NOT NULL, achete TINYINT(1) NOT NULL, INDEX IDX_839457EC7294869C (article_id), INDEX IDX_839457EC6B01D708 (listecourse_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE liste_course (id INT AUTO_INCREMENT NOT NULL, utilisateur_id INT DEFAULT NULL, nom VARCHAR(255) NOT NULL, INDEX IDX_27EF1A82FB88E14F (utilisateur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE liste_course_utilisateur (liste_course_id INT NOT NULL, utilisateur_id INT NOT NULL, INDEX IDX_A37D05EB4680FCB (liste_course_id), INDEX IDX_A37D05EBFB88E14F (utilisateur_id), PRIMARY KEY(liste_course_id, utilisateur_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE utilisateur (id INT AUTO_INCREMENT NOT NULL, pseudo VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', UNIQUE INDEX UNIQ_1D1C63B386CC499D (pseudo), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL, available_at DATETIME NOT NULL, delivered_at DATETIME DEFAULT NULL, INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E66C54C8C93 FOREIGN KEY (type_id) REFERENCES type (id)');
        $this->addSql('ALTER TABLE article_ajoute_liste_course ADD CONSTRAINT FK_839457EC7294869C FOREIGN KEY (article_id) REFERENCES article (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE article_ajoute_liste_course ADD CONSTRAINT FK_839457EC6B01D708 FOREIGN KEY (listecourse_id) REFERENCES liste_course (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE liste_course ADD CONSTRAINT FK_27EF1A82FB88E14F FOREIGN KEY (utilisateur_id) REFERENCES utilisateur (id)');
        $this->addSql('ALTER TABLE liste_course_utilisateur ADD CONSTRAINT FK_A37D05EB4680FCB FOREIGN KEY (liste_course_id) REFERENCES liste_course (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE liste_course_utilisateur ADD CONSTRAINT FK_A37D05EBFB88E14F FOREIGN KEY (utilisateur_id) REFERENCES utilisateur (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E66C54C8C93');
        $this->addSql('ALTER TABLE article_ajoute_liste_course DROP FOREIGN KEY FK_839457EC7294869C');
        $this->addSql('ALTER TABLE article_ajoute_liste_course DROP FOREIGN KEY FK_839457EC6B01D708');
        $this->addSql('ALTER TABLE liste_course DROP FOREIGN KEY FK_27EF1A82FB88E14F');
        $this->addSql('ALTER TABLE liste_course_utilisateur DROP FOREIGN KEY FK_A37D05EB4680FCB');
        $this->addSql('ALTER TABLE liste_course_utilisateur DROP FOREIGN KEY FK_A37D05EBFB88E14F');
        $this->addSql('DROP TABLE article');
        $this->addSql('DROP TABLE article_ajoute_liste_course');
        $this->addSql('DROP TABLE liste_course');
        $this->addSql('DROP TABLE liste_course_utilisateur');
        $this->addSql('DROP TABLE type');
        $this->addSql('DROP TABLE utilisateur');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
