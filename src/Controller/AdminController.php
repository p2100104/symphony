<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Type;
use App\Form\TypeFormType;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use App\Repository\TypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin')]
class AdminController extends AbstractController
{
    #[Route('/', name: 'app_admin_index')]
    public function index(): Response
    {
        return $this->render('admin/index.html.twig', [
        ]);
    }

    #[Route('/articles/', name: 'app_admin_manage_articles')]
    public function articles(ArticleRepository $articleRepository): Response
    {
        $articles = $articleRepository->findAll();
        return $this->render('admin/articles.html.twig', [
            'articles' => $articles
        ]);
    }

    #[Route('/articles/edit-{id}', name: 'app_admin_manage_articles_edit', methods: ['GET', 'POST'])]
    public function edit_article(Request $request, Article $article, ArticleRepository $articleRepository): Response
    {
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $articleRepository->save($article, true);
            $this->addFlash("success", "Les modifications ont bien été enregistré");

            return $this->redirectToRoute('app_admin_manage_articles_edit', ['id' => $article->getId()]);
        }

        return $this->renderForm('admin/edit_article.html.twig', [
            'title' => 'Editer un article',
            'article' => $article,
            'form' => $form
        ]);
    }
    #[Route('/articles/delete-{id}', name: 'app_admin_manage_articles_delete', methods: ['GET'])]
    public function delete_article(Request $request, Article $article, ArticleRepository $articleRepository) : Response
    {
        $articleRepository->remove($article, true);
        return $this->redirectToRoute('app_admin_manage_articles');
    }

    #[Route('/articles/new', name: 'app_admin_manage_articles_new', methods: ['GET', 'POST'])]
    public function new_article(Request $request, ArticleRepository $articleRepository): Response
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $article->setImage(file_get_contents($form->get("file")->getData()));

            $articleRepository->save($article, true);
            $this->addFlash("success", "Le produit a correctement été créé");

            return $this->redirectToRoute('app_admin_manage_articles_edit', ['id' => $article->getId()]);
        }

        return $this->renderForm('admin/new_article.html.twig', [
            'form' => $form
        ]);
    }

    #[Route('/type', name: 'app_admin_manage_type')]
    public function types(TypeRepository $typeRepository): Response
    {
        return $this->render('admin/type.html.twig', ['types' => $typeRepository->findAll(),]);
    }

    #[Route('/typeNew', name: 'app_admin_type_new', methods: ['GET', 'POST'])]
    public function typesnew(Request $request, TypeRepository $typeRepository): Response
    {
        $type = new Type();
        $form = $this->createForm(TypeFormType::class, $type);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $typeRepository->save($type, true);
            $this->addFlash("success", "Les modifications ont bien été enregistré");
            return $this->redirectToRoute('app_admin_manage_type', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('/admin/typeNew.html.twig', [
            'form' => $form,
        ]);
    }

     #[Route('/{id}/typeEdit', name: 'app_admin_type_edit', methods: ['GET', 'POST'])]
     public function typesedit( $id, Request $request, Type $type, TypeRepository $typeRepository): Response{
        $form = $this->createForm(TypeFormType::class, $type);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $typeRepository->save($type, true);
            $this->addFlash("success", "Les modifications ont bien été enregistré");
            return $this->redirectToRoute('app_admin_manage_type', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('/admin/typeEdit.html.twig', [
            'type' => $type,
            'form' => $form,
        ]);
     }

    #[Route('/{id}/typeDelete', name: 'app_admin_type_delete', methods: ['GET', 'POST'])]
    public function typesDelete($id, Request $request, Type $type, TypeRepository $typeRepository): Response{
        if ($this->isCsrfTokenValid('delete'.$type->getId(), $request->request->get('_token'))) {
            $typeRepository->remove($type, true);
        }
        return $this->redirectToRoute('app_admin_manage_type', [], Response::HTTP_SEE_OTHER);
    }
}
