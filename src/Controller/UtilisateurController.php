<?php

namespace App\Controller;

use App\Class\Search;
use App\Entity\ArticleAjouteListeCourse;
use App\Entity\ListeCourse;
use App\Form\AjouterCollaborateursType;
use App\Form\ArticleAjouteListeCourseType;
use App\Form\ListeCourseFormType;
use App\Form\SearchType;
use App\Repository\TypeRepository;
use App\Repository\ArticleRepository;
use App\Repository\ListeCourseRepository;
use App\Repository\ArticleAjouteListeCourseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

#[Route('/utilisateur')]
class UtilisateurController extends AbstractController
{
    #[Route('/', name: 'app_utilisateur')]
    public function index(ListeCourseRepository $listeCourseRepository): Response
    {
        list($liste_liste, $somme, 
        $nb_articles, $nb_articles_achetes, 
        $moyenne, $moins_cher, $plus_cher, 
        $article_favoris, $liste_types, 
        $nb_article_type) = $listeCourseRepository->getData($this->getUser()->getId());

        return $this->render('utilisateur/index.html.twig', [
            'user' => $this->getUser(),
            'liste_de_liste' => $liste_liste,
            'somme' => $somme,
            'nb_articles' => $nb_articles,
            'nb_articles_achetes' => $nb_articles_achetes,
            'moyenne' => round($moyenne,2),
            'moins_cher' => $moins_cher,
            'plus_cher' => $plus_cher,
            'favoris' => $article_favoris,
            'liste_types' => json_encode($liste_types),
            'nb_article_type' => json_encode($nb_article_type),
        ]);
    }

    #[Route('/liste/{id}', name: 'liste_utilisateur')]
    public function showList(ListeCourse $listecourse, ArticleAjouteListeCourseRepository $articleAjouteListeCourseRepository): Response
    {
        if ($listecourse->getUtilisateur() == $this->getUser()){
            return $this->render('utilisateur/liste.html.twig', [
                'liste' => $listecourse,
            ]);
        } else if ($listecourse->getCollaborateurs()->contains($this->getUser())){
            return $this->render('utilisateur/liste_invite.html.twig', [
                'liste' => $listecourse,
            ]);
        } else {
            throw $this->createAccessDeniedException("Pas ta liste !");
        }
    }


    #[Route('/creer-liste', name: 'creerliste', methods: ['GET', 'POST'])]
    public function createList(Request $request, ListeCourseRepository $listeCourseRepository): Response
    {
        $liste_course = new ListeCourse();
        $liste_course->setUtilisateur($this->getUser());
        $form = $this->createForm(ListeCourseFormType::class, $liste_course);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $listeCourseRepository->save($liste_course, true);

            return $this->redirectToRoute('liste_utilisateur', ['id' => $liste_course->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('utilisateur/creer_liste.html.twig', [
            'form' => $form,
        ]);
    }
    #[Route('/liste/{id}/ajouter-collaborateur', name:"liste_utilisateur_ajouter_collaborateur", methods: ['GET', 'POST'])]
    public function addCollaborateur(Request $request, ListeCourse $listeCourse, ListeCourseRepository $listeCourseRepository){
        $form = $this->createForm(AjouterCollaborateursType::class, $listeCourse);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $listeCourseRepository->save($listeCourse, true);

            return $this->redirectToRoute("liste_utilisateur", ['id' => $listeCourse->getId()]);

        }

        return $this->renderForm('utilisateur/ajouter_collaborateur.html.twig', [
            'form' => $form,
            'listecourse' => $listeCourse,
        ]);
    }

    #[Route('/liste/{id}/ajouter-article', name:"liste_utilisateur_ajouter_article", methods: ['GET', 'POST'])]
    public function addArticle(Request $request, ListeCourse $listeCourse, ArticleAjouteListeCourseRepository $articleAjouteListeCourseRepository, ArticleRepository $articleRepository) : Response
    {
        if ($listeCourse->getUtilisateur() != $this->getUser()){
            throw $this->createAccessDeniedException("Pas ta liste !");
        }

        $articleAjoute = new ArticleAjouteListeCourse();
        $articleAjoute->setListecourse($listeCourse);
        $articleAjoute->setAchete(false);

        $form = $this->createForm(ArticleAjouteListeCourseType::class, $articleAjoute);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $articleAjouteListeCourseRepository->save($articleAjoute, true);
            return $this->redirectToRoute("liste_utilisateur", ['id' => $listeCourse->getId()]);

        }

        $search = $request->query->get("search");
        $articles = $articleRepository->findByName($search);
        return $this->renderForm("utilisateur/ajouter_article.html.twig", [
            'form' => $form,
            'listecourse' => $listeCourse,
            'articles' => $articles
        ]);
    }


    #[Route('/supprimer-liste/{id}', name: 'supprimerliste', methods: ['GET', 'POST'])]
    public function deleteList(Request $request, ListeCourse $listecourse, ListeCourseRepository $listeCourseRepository, ArticleAjouteListeCourseRepository $articleAjouteListeCourseRepository): Response
    {
        if ($listecourse->getUtilisateur() != $this->getUser()){
            throw $this->createAccessDeniedException("Pas ta liste !");
        }

        if ($this->isCsrfTokenValid('delete'.$listecourse->getId(), $request->request->get('_token'))) {
            $listeCourseRepository->remove($listecourse, true);
        }   
        return $this->redirectToRoute('app_utilisateur', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/liste/{id_liste}/supprimer-article/{id_article}', name: 'supprimerArticleliste', methods: ['GET', 'POST'])]
    public function deleteListArticle(Request $request, ListeCourse $id_liste ,ArticleAjouteListeCourse $id_article, ArticleAjouteListeCourseRepository $articleAjouteListeCourseRepository): Response
    {
        if ($id_liste->getUtilisateur() != $this->getUser()){
            throw $this->createAccessDeniedException("Pas ta liste !");
        }

        if($this->isCsrfTokenValid('delete'.$id_article->getId(), $request->request->get('_token'))) {
            $articleAjouteListeCourseRepository->remove($id_article, true);
            
        }   
        return $this->redirectToRoute('liste_utilisateur', ['id' => $id_liste->getId()], Response::HTTP_SEE_OTHER);
    }

    #[Route('/liste/{id_liste}/acheter-article/{id_article}', name: 'acheterArticleliste', methods: ['GET'])]
    public function acheterListArticle(Request $request, ListeCourse $id_liste, ArticleAjouteListeCourse $id_article, ArticleAjouteListeCourseRepository $articleAjouteListeCourseRepository): Response
    {
        if (!($id_liste->getUtilisateur() == $this->getUser() || $id_liste->getCollaborateurs()->contains($this->getUser()))){
            throw $this->createAccessDeniedException("Pas ta liste !");
        }

        $id_article->setAchete(!$id_article->isAchete());
        $articleAjouteListeCourseRepository->save($id_article, true);
        return $this->redirectToRoute('liste_utilisateur', ['id' => $id_liste->getId()], Response::HTTP_SEE_OTHER);
    }
}