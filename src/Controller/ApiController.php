<?php

namespace App\Controller;

use App\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api')]
class ApiController extends AbstractController
{
    #[Route('/image/article/{id}', name: 'app_api_image_article')]
    public function index(Article $article): Response
    {
        $response = new Response();

        $image = $article->getImage();

        if ($image){
            $response->setContent(stream_get_contents($article->getImage()));
            $response->headers->set('Content-Type', 'image/png');
        } else {
            $response->setContent(null);
            $response->setStatusCode(404);
        }

        return $response;
    }
}
