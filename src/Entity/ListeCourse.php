<?php

namespace App\Entity;

use App\Repository\ListeCourseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ListeCourseRepository::class)]
class ListeCourse
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToMany(mappedBy: 'listecourse', targetEntity: ArticleAjouteListeCourse::class)]
    private Collection $articleAjouteListeCourses;

    #[ORM\ManyToOne(inversedBy: 'listeCourses')]
    private ?Utilisateur $utilisateur = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\ManyToMany(targetEntity: Utilisateur::class, inversedBy: 'listeCoursePartage')]
    private Collection $collaborateurs;

    public function __construct()
    {
        $this->articleAjouteListeCourses = new ArrayCollection();
        $this->collaborateurs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, ArticleAjouteListeCourse>
     */
    public function getArticleAjouteListeCourses(): Collection
    {
        return $this->articleAjouteListeCourses;
    }

    public function addArticleAjouteListeCourse(ArticleAjouteListeCourse $articleAjouteListeCourse): self
    {
        if (!$this->articleAjouteListeCourses->contains($articleAjouteListeCourse)) {
            $this->articleAjouteListeCourses->add($articleAjouteListeCourse);
            $articleAjouteListeCourse->setListecourse($this);
        }

        return $this;
    }

    public function removeArticleAjouteListeCourse(ArticleAjouteListeCourse $articleAjouteListeCourse): self
    {
        if ($this->articleAjouteListeCourses->removeElement($articleAjouteListeCourse)) {
            // set the owning side to null (unless already changed)
            if ($articleAjouteListeCourse->getListecourse() === $this) {
                $articleAjouteListeCourse->setListecourse(null);
            }
        }

        return $this;
    }

    public function getUtilisateur(): ?Utilisateur
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(?Utilisateur $utilisateur): self
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection<int, Utilisateur>
     */
    public function getCollaborateurs(): Collection
    {
        return $this->collaborateurs;
    }

    public function addCollaborateur(Utilisateur $collaborateur): self
    {
        if (!$this->collaborateurs->contains($collaborateur)) {
            $this->collaborateurs->add($collaborateur);
            $collaborateur->addListeCoursePartage($this);
        }

        return $this;
    }

    public function removeCollaborateur(Utilisateur $collaborateur): self
    {
        $this->collaborateurs->removeElement($collaborateur);
        $collaborateur->removeListeCoursePartage($this);

        return $this;
    }
}
