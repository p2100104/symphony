<?php

namespace App\Entity;

use App\Repository\UtilisateurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UtilisateurRepository::class)]
#[UniqueEntity(fields: ['pseudo'], message: 'There is already an account with this pseudo')]
class Utilisateur implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, unique : true)]
    private ?string $pseudo = null;

    #[ORM\Column(length: 255)]
    private ?string $password = null;

    #[ORM\Column(type: Types::ARRAY)]
    private array $roles = [];

    #[ORM\OneToMany(mappedBy: 'utilisateur', targetEntity: ListeCourse::class)]
    private Collection $listeCourses;

    #[ORM\ManyToMany(targetEntity: ListeCourse::class, mappedBy: 'collaborateurs')]
    private Collection $listeCoursePartage;

    public function __construct()
    {
        $this->listeCourses = new ArrayCollection();
        $this->listeCoursePartage = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->pseudo;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function __toString() : string{
        return $this->pseudo;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }


    /**
     * @return Collection<int, ListeCourse>
     */
    public function getListeCourses(): Collection
    {
        return $this->listeCourses;
    }

    public function addListeCourse(ListeCourse $listeCourse): self
    {
        if (!$this->listeCourses->contains($listeCourse)) {
            $this->listeCourses->add($listeCourse);
            $listeCourse->setUtilisateur($this);
        }

        return $this;
    }

    public function removeListeCourse(ListeCourse $listeCourse): self
    {
        if ($this->listeCourses->removeElement($listeCourse)) {
            // set the owning side to null (unless already changed)
            if ($listeCourse->getUtilisateur() === $this) {
                $listeCourse->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ListeCourse>
     */
    public function getListeCoursePartage(): Collection
    {
        return $this->listeCoursePartage;
    }

    public function addListeCoursePartage(ListeCourse $listeCourse): self
    {
        if (!$this->listeCoursePartage->contains($listeCourse)) {
            $this->listeCoursePartage->add($listeCourse);

        }

        return $this;
    }

    public function removeListeCoursePartage(ListeCourse $listeCourse): self
    {
        if ($this->listeCoursePartage->removeElement($listeCourse)) {
            // set the owning side to null (unless already changed)
            if ($listeCourse->getUtilisateur() === $this) {
            }
        }

        return $this;
    }

}
