<?php

namespace App\Entity;

use App\Repository\ArticleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ArticleRepository::class)]
class Article
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column]
    private ?float $prix = null;

    #[ORM\ManyToOne(inversedBy: 'articles')]
    #[ORM\JoinColumn(nullable: true)]
    private ?Type $type = null;

    #[ORM\OneToMany(mappedBy: 'article', targetEntity: ArticleAjouteListeCourse::class)]
    private Collection $articleAjouteListeCourses;

    #[ORM\Column(type: Types::BLOB, nullable: true)]
    private $image = null;

    public function __construct()
    {
        $this->articleAjouteListeCourses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function setType(?Type $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection<int, ArticleAjouteListeCourse>
     */
    public function getArticleAjouteListeCourses(): Collection
    {
        return $this->articleAjouteListeCourses;
    }

    public function addArticleAjouteListeCourse(ArticleAjouteListeCourse $articleAjouteListeCourse): self
    {
        if (!$this->articleAjouteListeCourses->contains($articleAjouteListeCourse)) {
            $this->articleAjouteListeCourses->add($articleAjouteListeCourse);
            $articleAjouteListeCourse->setArticle($this);
        }

        return $this;
    }

    public function removeArticleAjouteListeCourse(ArticleAjouteListeCourse $articleAjouteListeCourse): self
    {
        if ($this->articleAjouteListeCourses->removeElement($articleAjouteListeCourse)) {
            // set the owning side to null (unless already changed)
            if ($articleAjouteListeCourse->getArticle() === $this) {
                $articleAjouteListeCourse->setArticle(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->nom;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image): self
    {
        $this->image = $image;

        //$this->image = base64_encode(stream_get_contents($image));

        return $this;
    }
}
