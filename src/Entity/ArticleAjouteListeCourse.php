<?php

namespace App\Entity;

use App\Repository\ArticleAjouteListeCourseRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ArticleAjouteListeCourseRepository::class)]
class ArticleAjouteListeCourse
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $quantite = null;

    #[ORM\Column]
    private ?bool $achete = null;

    #[ORM\ManyToOne(inversedBy: 'articleAjouteListeCourses')]
    //#[ORM\JoinColumn(nullable: false)]
    #[ORM\JoinColumn(onDelete: "CASCADE")]
    private ?Article $article = null;

    #[ORM\ManyToOne(inversedBy: 'articleAjouteListeCourses')]
    //#[ORM\JoinColumn(nullable: false)]
    #[ORM\JoinColumn(onDelete: "CASCADE")]
    private ?ListeCourse $listecourse = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    public function setQuantite(int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function isAchete(): ?bool
    {
        return $this->achete;
    }

    public function setAchete(bool $achete): self
    {
        $this->achete = $achete;

        return $this;
    }

    public function getArticle(): ?Article
    {
        return $this->article;
    }

    public function setArticle(?Article $article): self
    {
        $this->article = $article;

        return $this;
    }

    public function getListecourse(): ?ListeCourse
    {
        return $this->listecourse;
    }

    public function setListecourse(?ListeCourse $listecourse): self
    {
        $this->listecourse = $listecourse;

        return $this;
    }
}
