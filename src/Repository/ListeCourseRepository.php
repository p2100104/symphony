<?php

namespace App\Repository;

use App\Entity\ListeCourse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ListeCourse>
 *
 * @method ListeCourse|null find($id, $lockMode = null, $lockVersion = null)
 * @method ListeCourse|null findOneBy(array $criteria, array $orderBy = null)
 * @method ListeCourse[]    findAll()
 * @method ListeCourse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ListeCourseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ListeCourse::class);
    }

    public function save(ListeCourse $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ListeCourse $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getData(string $pseudo)
    {
        $liste_liste = $this->findBy(['utilisateur' => $pseudo]);
        $somme = 0;
        $nb_articles = 0;
        $nb_articles_achetes = 0;
        $moyenne = 0;
        $plus_cher = 0;
        $moins_cher = 0;
        
        $quantitemax=0;
        $article_favoris ="";
        $dico_article = array();

        $liste_types = [];

        foreach($liste_liste as $liste){
            foreach($liste->getArticleAjouteListeCourses() as $articleAjoute){
                if(!in_array($articleAjoute->getArticle()->getType()->__toString(), $liste_types)){
                    array_push($liste_types, $articleAjoute->getArticle()->getType()->__toString());
                }
            }
        }

        $liste_types = array_map(fn($elt) => $elt, $liste_types);
        $nb_article_type = array_map(fn($elt) => 0, $liste_types);


        foreach($liste_liste as $liste)
        {
            foreach($liste->getArticleAjouteListeCourses() as $articleAjoute)
            {
                if(isset($dico_article[$articleAjoute->getArticle()->getNom()]))
                {
                    $dico_article[$articleAjoute->getArticle()->getNom()] += $articleAjoute->getQuantite();
                    
                }
                else 
                {
                    $dico_article[$articleAjoute->getArticle()->getNom()] = $articleAjoute->getQuantite();
                }
                


                $nb_articles = $nb_articles + $articleAjoute->getQuantite();
                foreach($liste_types as $index => $type){
                    if($type == $articleAjoute->getArticle()->getType()->__toString()){
                        $nb_article_type[$index] = $nb_article_type[$index] + 1 * $articleAjoute->getQuantite();
                    }
                }

                if($articleAjoute->getArticle()->getPrix() > $plus_cher){
                    $plus_cher = $articleAjoute->getArticle()->getPrix();
                }

                if($articleAjoute->getArticle()->getPrix() < $moins_cher || $moins_cher == 0){
                    $moins_cher = $articleAjoute->getArticle()->getPrix();
                }

                if($articleAjoute->isAchete())
                {
                    $somme=$somme + ($articleAjoute->getArticle()->getPrix())*$articleAjoute->getQuantite();
                    $nb_articles_achetes = $nb_articles_achetes + $articleAjoute->getQuantite();
                }
            }
        }


        foreach($dico_article as $article => $quantite)
        {
            if($quantite>$quantitemax)
            {
                $article_favoris=$article;
                $quantitemax=$quantite;
            }
        }

        $nb_article_type = array_map(fn($elt) => strval($elt), $nb_article_type);

        if($nb_articles_achetes <= 0){
            $moyenne = 0;
        } else {
            $moyenne = $somme / $nb_articles_achetes;
        }

        return array($liste_liste, $somme, 
        $nb_articles, $nb_articles_achetes, 
        $moyenne, $moins_cher, $plus_cher, 
        $article_favoris, $liste_types, 
        $nb_article_type);
    }
//    /**
//     * @return ListeCourse[] Returns an array of ListeCourse objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('l')
//            ->andWhere('l.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('l.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ListeCourse
//    {
//        return $this->createQueryBuilder('l')
//            ->andWhere('l.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
